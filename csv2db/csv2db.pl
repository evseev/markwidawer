#!/usr/bin/perl

use strict;
use warnings;

use Config::Tiny;
use Encode;

# Web part..
use LWP::UserAgent;
use HTML::Form;

# Mail part..
use Net::IMAP::Client;
use Email::MIME;

# TimeDate package..
use Date::Format;
use Date::Parse;

# Database part, see also DSN in $default_dbconfig below..
use DBI;
#use DBD::Sybase;
#use DBD::ODBC;

#=======================================================================

# Configs..

my $global_config = (@ARGV and -f $ARGV[0]) ? Config::Tiny->read(shift @ARGV) : {};
my   $main_config = $global_config->{_} || {};

my $default_webconfig = {
	url      => 'https://admin.liveauctiongroup.com/downloads/registrants.ashx?wsr=1',
	username => 'demologin',
	password => '123456',
	username_field  => 'txtUsername',
	password_field  => 'txtPassword',
	default_charset => 'Windows-1252',
	force_charset   => undef,
};

my $default_mailconfig = {
	ignore   => 1,
	server   => 'imap.yandex.ru',
	username => 'perltest@cyberity.ru',
	password => 'SystemAdmin33',
	ssl      => 1,
	maxdays  => 7,
	subject  => undef,
	getall   => undef,  # by default, the newest email is handled only
	default_charset => 'UTF-16LE',
	force_charset   => undef,
	search_charset  => undef,
};

my $default_dbconfig = {
	dsn    => ( ( $^O =~ /mswin/i ) ? 'DBI:ODBC:driver={SQL Server};Server'
	                                : 'DBI:Sybase:server' ),
	dbhost => 'w01.smtdpcloud.tk',
	dbuser => 'sa',
	dbpass => 'SystemAdmin22',
	dbname => 'perltest1',
};

sub merge_configs($$) {
	my $config = shift || {};
	my $default = shift;
	while (my ($key,$val) = each %$default)
		{ $config->{$key} = $val unless exists $config->{$key} }
	$config
}

my %regfields = (
	'Email'                => 0,
	'Username'             => 0,
	'First'                => 0,
	'Last'                 => 0,
	'MemberSince'          => 0,
	'Site Profiles'        => 0,
	'Site Member Since'    => 'SiteMemberSince',
	'Tracking Type'        => 0,
	'Tracking Target'      => 0,
	'Tracking DateCreated' => 0,
	'BillingAddress'       => 0,
	'BillingCity'          => 0,
	'BillingProvince'      => 0,
	'BillingCountry'       => 0,
	'BillingPostalCode'    => 0,
	'IPAddress'            => 0,
);

#=======================================================================

# Logging..

sub Log_verbose {
	return if not $main_config->{verbose};
	print time2str('%Y-%m-%d %H:%M:%S', time), " -- @_\n";
}

sub Log_debug {
	return if lc($main_config->{verbose} || '') ne 'debug';
	print time2str('%Y-%m-%d %H:%M:%S', time), " -- @_\n";
}

sub Log_error {
	print "ERROR: @_\n";
	return;
}

#=======================================================================

# Database...

sub dbconnect($)
{
	my $config = merge_configs(shift, $default_dbconfig);
	return if $config->{ignore};

	my $dsn = "$config->{dsn}=$config->{dbhost}";
	Log_verbose "Connect to ${dsn}, user=$config->{dbuser}...";
	my $dbh = DBI->connect($dsn, $config->{dbuser}, $config->{dbpass},
		{ PrintError => 0, RaiseError => 0 } );
	return Log_error "Cannot connect to database: ".$DBI::errstr."\n" unless $dbh;

	if ($config->{dbname}) {
		Log_verbose "Select database \"$config->{dbname}\"";
		return Log_error "Cannot use database \"$config->{dbname}\": ".$dbh->errstr
		   if not $dbh->do("use [$config->{dbname}]");
	}

	$dbh;
}

#=======================================================================

# Files..

sub decode_from($$$)
{
	my ($contents, $content_type, $config) = @_;
	my $charset = $config->{force_charset} || ( $content_type =~ /charset=([^;]+)/ ? $1 : $config->{default_charset} ) ;
	return $contents if not $charset;

	Log_verbose "Decode from $charset...";
	my $e = find_encoding($charset);
	return $e->decode($contents) if $e;

	Log_error "No support for $charset charset, try to use unconverted...";
	$contents;
}


sub insert_tstamp($) {
	local $_ = shift;
	my $t = time2str('%Y%m%d', time);
	return sprintf('%s_%s.%s', $1, $t, $2) if /^(.+)\.([^\.]*)$/;
	return sprintf('%s_%s', $_, $t);
}

sub write_files {
	my $files = shift or return;
	while (my ($fname, $contents) = each %$files) {
		$fname = insert_tstamp($fname);
		Log_verbose "Write \"$fname\"...";
		unless (open F, '>', $fname) {
			Log_error "Cannot write \"$fname\": ".$!;
			next;
		}
		print F $contents;
		close F;
		Log_verbose "File created.";
	}
	$files;
}

#=======================================================================

# Web..

sub fetch_webfile($)    # ..returns undef on error, or filename on success
{
	my $config = merge_configs(shift, $default_webconfig);
	return if $config->{ignore};

	my $a = LWP::UserAgent->new(cookie_jar => {});
	push @{ $a->requests_redirectable }, 'POST';

	Log_verbose "Request $config->{url}...";
	my $response = $a->get($config->{url});
	return Log_error "$response->status_line"
		if not $response->is_success;

	my @forms = HTML::Form->parse($response);
	return Log_error "Wrong forms count on page (".scalar @forms.")\n"
		if @forms != 1;

	my $form = $forms[0];
	$form->value( $config->{username_field} => $config->{username} );
	$form->value( $config->{password_field} => $config->{password} );

	Log_verbose "Submit form with username and password...";
	$response = $a->request($form->click);
	return Log_error "$response->status_line"
		if not $response->is_success;

	my $s = $response->header('Content-Disposition');
	return Log_error "Missing Content-Disposition " if not defined $s;
	return Log_error "Wrong Content-Disposition \"$s\""
		unless $s =~ /^attachment;filename=(.+)$/;
	my $fname = $1;
	Log_verbose "Attachment filename \"$fname\"";

	my %result = ( $fname => decode_from( $response->content, $response->header('Content-Type'), $config ) );
	\%result;
}

#=======================================================================

# Mail..

sub get_mailheader($$) {
	my ($name, $parts) = @_;
	my $found;
	foreach(@$parts) {
		return $_ if $found;
		$found = 1 if $_ eq $name;
	}
}

sub fetch_mailfiles($)
{
	my $config = merge_configs(shift, $default_mailconfig);
	return if $config->{ignore};

	Log_verbose "Connect to $config->{server}...";
	my $imap = Net::IMAP::Client->new(
		server => $config->{server},
		user   => $config->{username},
		pass   => $config->{password},
		ssl    => $config->{ssl},
	) or return Log_error "Could not connect to IMAP server";

	Log_verbose "Login as $config->{username}...";
	$imap->login or return Log_error 'Login failed: ' . $imap->last_error;

	Log_verbose "Inbox...";
	$imap->select('INBOX');

	my @criterias = qw/UNSEEN/;
	push @criterias, "SUBJECT \"$config->{subject}\""
		if defined $config->{subject};
	push @criterias, "SINCE \"".time2str('%d-%b-%Y', time - $config->{maxdays} * 86400)."\""
		if $config->{maxdays} and $config->{maxdays} > 0;
	Log_verbose "Search for: @criterias ...";
	my $msgids = $imap->search("@criterias", undef, $config->{search_charset});
	unless ($msgids) {
		Log_error "Search failed: " . $imap->last_error;
		$imap->logout();
		return;
	}
	my @sorted_ids = sort {$a <=> $b} @$msgids;

	Log_verbose "Fetch messages @sorted_ids...";
	my %results;
	foreach my $msgid (@sorted_ids) {
		Log_verbose "Fetch message $msgid...";
		my $data = $imap->get_rfc822_body($msgid);
		next if $msgid != $sorted_ids[$#sorted_ids] and not $config->{getall};

		Log_verbose "Parse message...";
		my $email = Email::MIME->new($data);
		$email->walk_parts(sub {
			my ($part) = @_;
			return if $part->subparts; # multipart

			my $fname = $part->filename or return;
			Log_verbose "Found attachment \"$fname\"";
			$results{$fname} = decode_from($part->body, $part->content_type, $config);
		});
	}

	Log_verbose "Disconnect...";
	$imap->logout();
	\%results;
}

#=======================================================================

# Parse files..

sub str2sqldate($) {
	my ($ss,$mm,$hh,$day,$month,$year,$zone) = strptime($_[0])
		or return;
	sprintf('%d-%02d-%02d', $year + 1900, $month + 1, $day);
}

sub str2sqltime($) {
	my ($ss,$mm,$hh,$day,$month,$year,$zone) = strptime($_[0])
		or return;
	sprintf('%d-%02d-%02d %02d:%02d:%02d', $year + 1900, $month + 1, $day, $hh || 0, $mm || 0, $ss || 0);
}

sub str2sqlmoney($) {
	local $_ = shift;
	s/\$//;
	s/,//g;
	s/\./,/;
	"'$_'";
}

sub parse_csv($) {
	my $text = shift;
	my @fields = ();
	push(@fields, $+) while $text =~ m{
		"([^\"\\]*(?:\\.[^\"\\]*)*)",?
		| ([^,]+),?
		| ,
	}gx;
	push(@fields, undef) if substr($text, -1, 1) eq ',';
	return @fields;
}

sub parse_adwords($$$)
{
	my ($dbh, $table, $contents) = @_;
	my @lines    = split /[\r\n]+/, $contents;
	my $header   = shift @lines;
	my @names    = split /\t/, $header;
	my $csv;

	if (@names == 1) {
		Log_error "Seems not TAB-separated, switch to CSV mode!";
		$csv = 1;
		@names = parse_csv($header);
	}

	foreach(@names) { $_ = 'Date' if $_ eq 'Day' }

	Log_verbose "Adwords fields: @names";

	my @query = ( "INSERT INTO $table(".join(', ', @names).") VALUES" );
	my $comma = ' ';
	my $mintime = 0;

	#  Todo: support multiple files.
	#  Todo: validate column names, skip unknown.
	#  Todo: convert date and money based on column name.

	foreach my $line (@lines) {
		my @a = $csv ? parse_csv($line) : split /\t/, $line;
		#Log_verbose "Words In = ".join(' -- ', @a);
		foreach (@a) {
			$_ = $1 if /^"(.*)"$/;
			if (/, \d\d\d\d$/) {            # ..seems like date?
				my $t = str2time($_);
				$_ = "'".str2sqldate($_)."'";
				$mintime = $t if !$mintime or $mintime > $t;
			} elsif (/^\$([\d,\.]+)$/) {  # ..seems like money?
				$_ = str2sqlmoney($_);
			} elsif (/^(\d+),(\d+)$/) {     # ..seems like float?
				$_ = "$1.$2";
			}
		}
		#Log_verbose "Words Out= ".join(' -- ', @a);
		push @query, "$comma(" . join(', ', @a) . ")";
		$comma = ',';
	}

	if ($mintime) {
		$mintime = time2str('%Y-%m-%d', $mintime);
		Log_verbose "Delete from $table all newer than $mintime...";
		return Log_error "Delete from $table newer that $mintime failed: ".$dbh->errstr
		   if not $dbh->do("DELETE FROM $table WHERE Date >= '$mintime'");
	}

	Log_verbose "Insert new records into $table...";
	Log_debug   "Query:\n\t", join("\n\t", @query);
	$dbh->do(join(" ", @query)) and return;
	Log_error "Insert into $table failed: ".$dbh->errstr;
}

sub parse_registrations($$$$)
{
	my ($dbh, $table, $filter, $contents) = @_;
	my @lines  = split /[\r\n]+/, $contents;
	my $header   = shift @lines;
	my @names    = split /,/, $header;
	my %filters  = map { $_ => 1 } split /\s*,\s*/, (defined $filter ? $filter : '');
	my $inserts  = 0;

	Log_verbose "Site Profiles: ".( %filters ? join(', ', keys %filters) : 'ALL' );

	Log_verbose "Delete all from $table...";
	return Log_error "Delete from $table failed: ".$dbh->errstr
	   if not $dbh->do("DELETE FROM $table");

	Log_verbose "Insert records into $table...";
LINE:	foreach (@lines) {
		my @w = parse_csv($_);
		my $n = ($#w < $#names) ? $#w : $#names;
		my @values;
		my @names2;

		foreach my $i (0 .. $#names) {
			my $name  = $names[$i];
			next if not exists $regfields{$name};
			$name = $regfields{$name} if $regfields{$name};

			my $value = $w[$i];
			if ($name eq 'Site Profiles') {
				if (%filters) {
					next LINE if not defined $value;
					next LINE if not $filters{$value};
				}
			} elsif (!defined $value) {
			} elsif ($name eq 'Tracking Type') {
				$value = $1 if $value =~ /^=(.*)$/;
			} elsif ($name eq 'Tracking DateCreated') {
				$value = str2sqldate($value);
			} elsif ($name eq 'SiteMemberSince') {
				$value = str2sqldate($value);
			} elsif ($name eq 'MemberSince') {
				$value = str2sqltime($value);
			} else {
				$value =~ s/\'/\'\'/;
			}
			#printf "\t%2d. %s = %s\n", $i, $name, $value;
			push @names2, $name;
			push @values, $value;
		}
		my $query = "INSERT INTO $table("
			. join(',', map { "[$_]" } @names2)
			. ") VALUES("
			. join( ',', map { defined $_ ? "'$_'" : 'NULL' } @values )
			. ")";
		Log_debug "Exec: $query";
		return Log_error "Query failed: ".$dbh->errstr."\n$query"
		   if not $dbh->do($query);
		$inserts++;
	}
	Log_verbose "Total $inserts records added.";
}

#=======================================================================

# Parse cmdline and merge to configs...

foreach (@ARGV) {
	if (/^--([[:alnum:]]+)=(.+)$/) {
		$main_config->{$1} = $2;
	} elsif (/^--([[:alnum:]]+):([[:alnum:]_]+)=(.+)$/) {
		$global_config->{$1}->{$2} = $3;
	} elsif (/^--([[:alnum:]]+)$/) {
		$main_config->{$1} = 1;
	} elsif (/^--([[:alnum:]_]+):([[:alnum:]]+)$/) {
		$global_config->{$1}->{$2} = 1;
	} else {
		die "Wrong cmdline argument: $_\n";
	}
}

#=======================================================================

my $dbh = dbconnect($global_config->{db}) or die "Cannot continue without working database connection\n";

sub get_first_content {
	my $hashref = shift      or return;
	my @v = values %$hashref or return;
	shift @v                 or return;
}

sub testfile_content($) {
	my $fname = shift->{input_file} or return;
	Log_verbose "Read testing data from $fname...";
	open F, $fname or return Log_error "Cannot open $fname: $!\n";
	my @lines = <F>;
	close F;
	Log_verbose "Readed ".(scalar @lines)." lines.";
	return join('', @lines);
}

my $w = $global_config->{web}  || {};
my $m = $global_config->{mail} || {};

my $registrations = testfile_content($w) || get_first_content( write_files( fetch_webfile  ($w)));
my $adwords       = testfile_content($m) || get_first_content( write_files( fetch_mailfiles($m)));

my $profiles = $w->{siteprofiles} || undef;

parse_adwords      ($dbh, 'PPCAdwordsData',              $adwords      ) if defined $adwords;
parse_registrations($dbh, 'PPCregistrations', $profiles, $registrations) if defined $registrations;

Log_verbose "Done.";

## END ##
