#!/usr/bin/perl

use strict;
use warnings;

use Date::Parse;
use Date::Format;

#y $contents = <>;
my @lines    = <>;
s/[\r\n]+$// foreach @lines;
my $header   = shift @lines;
my @names    = split /\t/, $header;
foreach(@names) { $_ = 'Date' if $_ eq 'Day' }

print "Names:\n";
print "\t$_\n" foreach @names;

sub str2sqltime($) {
	my ($ss,$mm,$hh,$day,$month,$year,$zone) = strptime($_[0])
		or return;
	sprintf('%d-%02d-%02d', $year + 1900, $month + 1, $day);
}

my @query = ( "INSERT INTO Table1(".join(', ', @names).") VALUES" );
my $comma = ' ';
my $mintime = 0;

foreach my $line (@lines) {
	my @a = split /\t/, $line;
	#print "Record:\n";
	foreach (@a) {
		$_ = $1 if /^"(.*)"$/ or /^\$(.+)$/;
		if (/, \d\d\d\d$/) {
			my $t = str2time($_);
			$_ = str2sqltime($_);
			$mintime = $t if !$mintime or $mintime > $t;
		}
		#print "\t$_\n";
	}
	push @query, "$comma(" . join(', ', map { '"'.$_.'"' } @a) . ")";
	$comma = ',';
}

print "Mintime: ", time2str('%Y-%m-%d', $mintime), "\n";

print "Query:\n", join("\n", @query), ";\n";

## END ##
