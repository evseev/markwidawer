#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use HTML::Form;

my $url = 'https://admin.liveauctiongroup.com/downloads/registrants.ashx?wsr=1';
my $username = 'demologin';
my $userpass = '123456';

# Based on http://www.herongyang.com/JSP/Session-Using-Perl-LWP-Package-for-Debugging.html
my $a = LWP::UserAgent->new(cookie_jar => {});
$a->add_handler( "request_send",  sub { print      "Request send:\n====\n"; shift->dump; print "\n====\n"; return } );
$a->add_handler( "response_done", sub { print "Response received:\n====\n"; shift->dump; print "\n====\n"; return } );
push @{ $a->requests_redirectable }, 'POST';

print "Get...\n";
my $r = $a->get($url);
die "ERROR: ", $r->status_line, "\n" if not $r->is_success;
print "...OK\n";

my $form = HTML::Form->parse($r)
	or die "Missing form\n";
$form->value(txtUsername => $username);
$form->value(txtPassword => $userpass);

print "Submit...\n";
$r = $a->request($form->click);
die "ERROR: ", $r->status_line, "\n" if not $r->is_success;
print "...OK\n";

print "Content-type = ", $r->header('Content-Type'),        "\n";
print "Content-disp = ", $r->header('Content-Disposition'), "\n";
print $r->content,"\n";

## END ##
