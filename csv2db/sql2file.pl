#!/usr/bin/perl
#
# DSN Reference:
#   http://stackoverflow.com/a/4905660/2743554
# Sybase guide:
#   http://www.perlmonks.org/?node_id=392385
#

use strict;
use warnings;

use DBI;

my $dbhost = "w01.smtdpcloud.tk";
my $dbname = "perltest1";
my $dbuser = "sa";
my $dbpass = "SystemAdmin22";

print "Connect...\n";
#y $dbh = DBI->connect("DBI:ODBC:driver={SQL Server}; Server=$dbhost", $dbuser, $dbpass, { RaiseError => 0, PrintError => 0 } )
my $dbh = DBI->connect("DBI:Sybase:server=$dbhost", $dbuser, $dbpass)
	or die "Cannot connect to database: ".$DBI::errstr."\n";

print "Set database...\n";
$dbh->do("USE [$dbname]");

sub exec_query {
	my $sth = $dbh->prepare(@_) or die "Cannot prepare $_[0]: ".$DBI::errstr."\n";
	$sth->execute()             or die "Cannot execute $_[0]: ".$DBI::errstr."\n";
	$sth
}

print "Query...\n";
my $sth = exec_query("select * from PPCregistrations");
print join("\t", map { defined $_ ? $_ : '<undef>' } @$_),"\n"
	while $_ = $sth->fetchrow_arrayref;

print "Disconnect...\n";
$dbh->disconnect;

## END ##
