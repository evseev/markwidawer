#!/usr/bin/perl

use strict;
use warnings;

use WWW::Mechanize;

my $url = 'https://admin.liveauctiongroup.com/downloads/registrants.ashx?wsr=1';
my $username = 'demologin';
my $userpass = '123456';

my $mech = WWW::Mechanize->new(agent => 'Windows IE 6', cookie_jar => {});
my $response;
#$mech->agent_alias('Windows IE 6');

print "Get...\n";
$mech->get($url);
$response = $mech->response;
if ($response->is_success) {
	print "...OK\n"; # $response->decoded_content;
} else {
	print "ERROR:", $response->status_line, "\n";
}
$mech->dump_headers;

my $form = $mech->current_form();
my %fields = map { $_->name => $_->value } $form->inputs;
$fields{txtUsername} = $username;
$fields{txtPassword} = $userpass;
print "$_ = $fields{$_}\n" foreach sort keys %fields; #exit;

print "Submit...\n";
$mech->submit_form(with_fields => \%fields);

if ($response->is_success) {
	print $response->decoded_content;
} else {
	print "ERROR:", $response->status_line, "\n";
}
$mech->dump_headers;
#print $mech->content;

## END ##
