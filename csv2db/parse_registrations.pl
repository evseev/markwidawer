#!/usr/bin/perl

use strict;
use warnings;

use Date::Parse;
use Date::Format;

my %regfields = (
	'Email'                => 0,
	'Username'             => 0,
	'First'                => 0,
	'Last'                 => 0,
	'MemberSince'          => 0,
	'Site Profiles'        => 0,
	'Site Member Since'    => 'SiteMemberSince',
	'Tracking Type'        => 0,
	'Tracking Target'      => 0,
	'Tracking DateCreated' => 0,
	'BillingAddress'       => 0,
	'BillingCity'          => 0,
	'BillingProvince'      => 0,
	'BillingCountry'       => 0,
	'BillingPostalCode'    => 0,
	'IPAddress'            => 0,
);

my @lines    = <>;
s/[\r\n]+$// foreach @lines;
my $header   = shift @lines;
my @names    = split /,/, $header;

foreach(@names) {
	next unless exists $regfields{$_};
	$_ = $regfields{$_} if $regfields{$_};
}

sub str2sqldate($) {
	my ($ss,$mm,$hh,$day,$month,$year,$zone) = strptime($_[0])
		or return;
	sprintf('%d-%02d-%02d', $year + 1900, $month + 1, $day);
}

sub str2sqltime($) {
	my ($ss,$mm,$hh,$day,$month,$year,$zone) = strptime($_[0])
		or return;
	sprintf('%d-%02d-%02d %02d:%02d:%02d', $year + 1900, $month + 1, $day, $hh || 0, $mm || 0, $ss || 0);
}

# http://www.perlmonks.org/?node_id=5722
sub parse_csv {
	my $text = shift; ## record containing comma-separated values
	my @new = ();
	push(@new, $+) while $text =~ m{
		## the first part groups the phrase inside the quotes
		"([^\"\\]*(?:\\.[^\"\\]*)*)",?
		| ([^,]+),?
		| ,
	}gx;
	push(@new, undef) if substr($text, -1,1) eq ',';
	return @new; ## list of values that were comma-spearated
}

foreach (@lines) {
	my @w = parse_csv($_);
	my $n = ($#w < $#names) ? $#w : $#names;
	my @values;
	my @names2;
	print "Record (fields=$#w, names=$#names):\n";
	#print join(' -- ', @w), "\n\n"; next;
	foreach my $i (0 .. $#names) {
		my $name  = $names[$i];
		next if not exists $regfields{$name};

		my $value = $w[$i];
		if (!defined $value) {
		} elsif ($name eq 'Tracking Type') {
			$value = $1 if $value =~ /^=(.*)$/;
		} elsif ($name eq 'Tracking DateCreated') {
			$value = str2sqldate($value);
		} elsif ($name eq 'SiteMemberSince') {
			$value = str2sqldate($value);
		} elsif ($name eq 'MemberSince') {
			$value = str2sqltime($value);
		}
		printf "\t%2d. %s = %s\n", $i, $name, defined $value ? $value : 'NULL';
		push @names2, $name;
		push @values, $value;
	}
	#print "INSERT INTO table(".join(',', @names2).") VALUES(".join( ',', map { defined $_ ? "'$_'" : 'NULL' } @values ).");\n\n";
}

## END ##
