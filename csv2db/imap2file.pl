#!/usr/bin/perl

use strict;
use warnings;

use Net::IMAP::Client;
use Email::MIME;
use Date::Format;  # POSIX::strftime cannot be used because it's locale-dependent
use Data::Dumper;

print "Connect...\n";
my $imap = Net::IMAP::Client->new(
	server => 'imap.yandex.ru',
	user   => 'perltest@cyberity.ru',
	pass   => 'SystemAdmin33',
	ssl    => 1,
#	port   => 993,
) or die "Could not connect to IMAP server";

print "Login...\n";
$imap->login or die('Login failed: ' . $imap->last_error);

print "Inbox...\n";
$imap->select('INBOX');

print "Search...\n";   # Filters list:  http://php.net/manual/en/function.imap-search.php
#y $criteria = 'UNSEEN SUBJECT "Test 1" SINCE "'.time2str('%d-%b-%Y', time - 400000).'"';
my $criteria = 'ALL';
my $messages = $imap->search($criteria);  # ..fetch message ids (as array reference)

sub getheader($$) {
	my ($name, $parts) = @_;
	my $found;
	foreach(@$parts) {
		return $_ if $found;
		$found = 1 if $_ eq $name;
	}
}

print "List...\n";
foreach my $msgid (@$messages)
{
	print "Message ID = $msgid\n";
	my $data = $imap->get_rfc822_body($msgid);
	my $email = Email::MIME->new($data);
	my @headers = $email->header_pairs;
	#rint "Headers:\n". Dumper(\@headers) . "\n";
	#rint "Message Subject = ", getheader('Subject', \@headers), "\n";
	print "Message Date = ",    getheader('Date',    \@headers), "\n";

	$email->walk_parts(sub {
		my ($part) = @_;
		return if $part->subparts; # multipart

		my $fname = $part->filename;
		print "Filename = $fname\n" if defined $fname;
		print "Content-type = ", $part->content_type, "\n";
		print "Content:\n", substr($part->body, 0, 72), "\n\n";
	});
}

# Close the IMAP connection
$imap->logout();

## END ##
